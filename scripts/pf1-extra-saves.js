var PF1Saves = PF1Saves || {};

(function (PF1Saves) {
    "use strict";
    const PACKAGE_ID = "pf1-extra-saves";

    // Register DevMode Log Flag
    Hooks.once('devModeReady', ({ registerPackageDebugFlag }) => {
        registerPackageDebugFlag(PACKAGE_ID);
    });

    // Log using PF1Saves.log(false, ..args)
    PF1Saves.log = function (force, ...args) {
        const shouldLog = force || game.modules.get('_dev-mode')?.api?.getPackageDebugValue(PACKAGE_ID);

      if (shouldLog) {
          console.log(PACKAGE_ID, '|', args);
      }
  }

    // Edit CONFIG.PF1
    Hooks.once("pf1PostInit", () => {
        CONFIG.PF1.savingThrows = {
            pow: `Power`,
            fort: `Fortitude`,
            ref: `Reflex`,
            disc: `Discretion`,
            will: `Will`,
            det: `Determination`,
        };
        CONFIG.PF1.buffTargets.pow = { label: "Power", category: "savingThrows" };
        CONFIG.PF1.buffTargets.disc = { label: "Discretion", category: "savingThrows" };
        CONFIG.PF1.buffTargets.det = { label: "Determination", category: "savingThrows" };
        CONFIG.PF1.contextNoteTargets.pow =  { label: "Power", category: "savingThrows" };
        CONFIG.PF1.contextNoteTargets.disc = { label: "Discretion", category: "savingThrows" };
        CONFIG.PF1.contextNoteTargets.det = { label: "Determination", category: "savingThrows" };

    });

    // Standard Change Targets
    Hooks.on("pf1GetChangeFlat", (target, modifier, result) => {
        switch (target) {
        case "pow":
            result.push("system.attributes.savingThrows.pow.total");
            break;
        case "disc":
            result.push("system.attributes.savingThrows.disc.total");
            break;
        case "det":
            result.push("system.attributes.savingThrows.det.total");
            break;
        default:
    }
    });

    Hooks.on("pf1PrepareBaseActorData", (actor) => {
        actor.system.attributes.savingThrows = {
            "fort": {
                "base": 0,
                "ability": "con",
                "total": 0
            },
            "ref": {
                "base": 0,
                "ability": "dex",
                "total": 0
            },
            "will": {
                "base": 0,
                "ability": "wis",
                "total": 0
            },
            "pow": {
                "base": 0,
                "ability": "str",
                "total": 0
            },
            "disc": {
                "base": 0,
                "ability": "int",
                "total": 0
            },
            "det": {
                "base": 0,
                "ability": "cha",
                "total": 0
            }
        };
        const classes = actor.items.filter((o) => o.type === "class");
        PF1Saves.log(false, "Class List",classes);
    });

    Hooks.on("pf1AddDefaultChanges", (actor, changes) => {
        const saves = actor.system.attributes.savingThrows;
        const powAbility = saves.pow.ability;
        changes.push(
            new globalThis.pf1.components.ItemChange({
                formula: getAbilityMod(powAbility),
                operator: "function",
                target: "savingThrows",
                subTarget: "pow",
                modifier: "untypedPerm",
                flavor: CONFIG.PF1.abilities[powAbility],
            })
        );
        getSourceInfo(actor.sourceInfo, "system.attributes.savingThrows.pow.total").positive.push({
            formula: `@abilities.${powAbility}.mod`,
            name: CONFIG.PF1.abilities[powAbility],
        });

        const discAbility = saves.disc.ability;
        changes.push(
            new globalThis.pf1.components.ItemChange({
                formula: getAbilityMod(discAbility),
                operator: "function",
                target: "savingThrows",
                subTarget: "disc",
                modifier: "untypedPerm",
                flavor: CONFIG.PF1.abilities[discAbility],
            })
        );
        getSourceInfo(actor.sourceInfo, "system.attributes.savingThrows.disc.total").positive.push({
            formula: `@abilities.${discAbility}.mod`,
            name: CONFIG.PF1.abilities[discAbility],
        });
        const detAbility = saves.det.ability;
        changes.push(
            new globalThis.pf1.components.ItemChange({
                formula: getAbilityMod(detAbility),
                operator: "function",
                target: "savingThrows",
                subTarget: "det",
                modifier: "untypedPerm",
                flavor: CONFIG.PF1.abilities[detAbility],
            })
        );
        getSourceInfo(actor.sourceInfo, "system.attributes.savingThrows.det.total").positive.push({
            formula: `@abilities.${detAbility}.mod`,
            name: CONFIG.PF1.abilities[detAbility],
        });
    });

    const getAbilityMod = function (ability) {
        return function (d) {
            return d.abilities[ability]?.mod ?? 0;
        };
    };

    const getSourceInfo = function (obj, key) {
        if (!obj[key]) {
            obj[key] = { negative: [], positive: [] };
        }
        return obj[key];
    };

    Hooks.once("pf1PostSetup", () => {
        libWrapper.register(PACKAGE_ID, 'globalThis.pf1.documents.item.ItemClassPF.prototype.prepareDerivedData', addItemWrapper, 'WRAPPER');
    });

    async function addItemWrapper(fnext, options = {}) {
        const itemData = this.system;
        if (itemData.savingThrows) {
            itemData.savingThrows.pow = itemData.savingThrows.pow || itemData.savingThrows.fort || {value: "low"} ;
            itemData.savingThrows.disc = itemData.savingThrows.disc || itemData.savingThrows.ref || { value: "low" };
            itemData.savingThrows.det = itemData.savingThrows.det || itemData.savingThrows.will || { value: "low" };
        }
        fnext(options);
    }
} ) (PF1Saves);
