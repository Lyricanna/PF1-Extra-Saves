# Extra Saves for Pathfinder 1e

Have you ever wished for more saving throws?
Do you just hate having to roll ability scores straight?
You ever think that the Barbarian being worse at crashing through a door than the wizard just because of a low roll is silly?  

This is the module for you.  

I've added three extra saving throws for Strength, Intelegence, and Charisma.  Either use them for spells and effects, or just as a way to add your standard modifers to what would otherwise be an Ability Check.  

## New Saves
Here's a list of the new saves and what I personally use them for.
### Power - POW
Power Saves are used for effects that would stop a combatant from moving or can be overcome with pure might.  
Common uses include bursting through a door or spells like Hold Person.  

### Discretion  - DISC
Discretion Saves represent quick thinking, logical deduction, and mental recall.  
Common uses include recalling important events, spoting a trap activating, or reconizing an illusion.  

### Determination - DET
Determination Saves represent a character's raw presence and tennacity.  (Willpower instead is self-control and emotional stability.)  
Common uses include enchantment spells and necromancy effects.

Manifest Link: https://gitlab.com/Lyricanna/PF1-Extra-Saves/-/raw/master/module.json

Warning: Disabling this module after using it is known to prevent Actors from loading properly.  Please backup your actors regularly.